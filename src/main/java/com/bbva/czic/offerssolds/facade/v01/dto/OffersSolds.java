
package com.bbva.czic.offerssolds.facade.v01.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "offersSolds", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlType(name = "offersSolds", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class OffersSolds
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique customer identifier.", required = true)
    private String id;
    @ApiModelProperty(value = "Indicators contain information of the transaction in the payment gateway", required = true)
    private List<Sellers> sellers;
    @ApiModelProperty(value = "Indicators contain information of the transaction in the payment gateway", required = true)
    private Offer offer;

    public OffersSolds() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Sellers> getSellers() {
        return sellers;
    }

    public void setSellers(List<Sellers> sellers) {
        this.sellers = sellers;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

}
