
package com.bbva.czic.offerssolds.business.dto;

import java.util.List;



public class DTOIntOffersSolds {

    public final static long serialVersionUID = 1L;
    private String id;
    private List<DTOIntSellers> sellers;
    private DTOIntOffer offer;

    public DTOIntOffersSolds() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<DTOIntSellers> getSellers() {
        return sellers;
    }

    public void setSellers(List<DTOIntSellers> sellers) {
        this.sellers = sellers;
    }

    public DTOIntOffer getOffer() {
        return offer;
    }

    public void setOffer(DTOIntOffer offer) {
        this.offer = offer;
    }

}
