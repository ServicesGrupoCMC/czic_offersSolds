
package com.bbva.czic.offerssolds.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "offer", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlType(name = "offer", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Offer
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique offer identifier.", required = true)
    private String id;
    @ApiModelProperty(value = "Name associated with the offer.", required = true)
    private String name;
    @ApiModelProperty(value = "Identity document information related to the seller.", required = true)
    private Product product;

    public Offer() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
