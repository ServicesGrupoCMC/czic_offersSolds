
package com.bbva.czic.offerssolds.business.dto;




public class DTOIntContractingChannel {

    public final static long serialVersionUID = 1L;
    private DTOIntType type;
    private String id;

    public DTOIntContractingChannel() {
        //default constructor
    }

    public DTOIntType getType() {
        return type;
    }

    public void setType(DTOIntType type) {
        this.type = type;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
