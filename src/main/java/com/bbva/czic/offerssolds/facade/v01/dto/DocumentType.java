
package com.bbva.czic.offerssolds.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "documentType", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlType(name = "documentType", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentType
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identifier document type identity.", required = true)
    private String id;
    @ApiModelProperty(value = "Description document type identity.", required = true)
    private String name;

    public DocumentType() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
