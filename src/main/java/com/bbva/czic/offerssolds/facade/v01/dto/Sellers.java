
package com.bbva.czic.offerssolds.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "sellers", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlType(name = "sellers", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Sellers
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Full name of the seller.", required = true)
    private String fullname;
    @ApiModelProperty(value = "Identity document information related to the seller.", required = true)
    private IdentityDocument identityDocument;

    public Sellers() {
        //default constructor
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public IdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(IdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

}
