
package com.bbva.czic.offerssolds.business.dto;

import java.util.List;



public class DTOIntProduct {

    public final static long serialVersionUID = 1L;
    private String contractId;
    private List<DTOIntContractingChannel> contractingChannel;

    public DTOIntProduct() {
        //default constructor
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public List<DTOIntContractingChannel> getContractingChannel() {
        return contractingChannel;
    }

    public void setContractingChannel(List<DTOIntContractingChannel> contractingChannel) {
        this.contractingChannel = contractingChannel;
    }

}
