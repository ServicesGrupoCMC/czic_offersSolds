
package com.bbva.czic.offerssolds.business.dto;




public class DTOIntOffer {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;
    private DTOIntProduct product;

    public DTOIntOffer() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DTOIntProduct getProduct() {
        return product;
    }

    public void setProduct(DTOIntProduct product) {
        this.product = product;
    }

}
