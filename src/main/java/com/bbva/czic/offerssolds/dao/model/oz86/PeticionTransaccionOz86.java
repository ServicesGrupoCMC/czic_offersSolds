package com.bbva.czic.offerssolds.dao.model.oz86;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>OZ86</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionOz86</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionOz86</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: OZWEOZ86.CCT
 * OZ86Creacion Pago                       OZ        OZ86-ECBVDKNPO OZWEOZ86            OZ86  NS3000NNNNNN    SSTN    C   SNNSSNNN  NN                2015-05-29CE15505 2015-11-2709.55.39CICSDC112015-05-29-10.56.29.766588CE15505 0001-01-010001-01-01
 * FICHERO: OZWEOZ86.FDF
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|01|00001|CONTRAT   |                 |X|020|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|02|00021|TIPCANL   |                 |X|002|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|03|00023|NOMCANL   |                 |X|030|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|04|00053|VALCANL   |                 |X|002|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|05|00055|TIPSCAN   |                 |X|002|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|06|00057|NOMSCAN   |                 |X|030|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|07|00087|VALSCAN   |                 |X|002|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|08|00089|CAMPANA   |                 |X|012|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|09|00111|NOMCAMP   |                 |X|030|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|10|00141|TPDOCU1   |                 |X|003|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|11|00144|NMDOCU1   |                 |X|015|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|12|00159|DGDOCU1   |                 |X|001|0|R|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|13|00160|TPDOCU2   |                 |X|003|0|O|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|14|00163|NMDOCU2   |                 |X|015|0|O|        |
 * OZWEOZ86|E-FORMATO ENTRADA CONSULTA    |F|05|00034|15|00178|DGDOCU2   |                 |X|001|0|O|        |
</pre></code>
 * 
 * @see RespuestaTransaccionOz86
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "OZ86",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionOz86.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoOZWEOZ86.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionOz86 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}