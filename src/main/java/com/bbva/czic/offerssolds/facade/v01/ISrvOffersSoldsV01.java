package com.bbva.czic.offerssolds.facade.v01;

import java.util.List;
import javax.ws.rs.core.Response;

import com.bbva.czic.offerssolds.facade.v01.dto.Sellers;
import com.bbva.czic.offerssolds.facade.v01.dto.Product;
import com.bbva.czic.offerssolds.facade.v01.dto.IdentityDocument;
import com.bbva.czic.offerssolds.facade.v01.dto.DocumentType;
import com.bbva.czic.offerssolds.facade.v01.dto.ContractingChannel;
import com.bbva.czic.offerssolds.facade.v01.dto.Offer;
import com.bbva.czic.offerssolds.facade.v01.dto.Type;
import com.bbva.czic.offerssolds.facade.v01.dto.OffersSolds;


public interface ISrvOffersSoldsV01 {
 	public Response createOfferSold(OffersSolds infoOffersSolds);

	
}