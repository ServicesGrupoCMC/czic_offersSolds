package com.bbva.czic.offerssolds.dao;

import com.bbva.czic.offerssolds.business.dto.DTOIntOffersSolds;

public interface OffersSoldsDAO {

	public void createOfferSold(DTOIntOffersSolds infoOffersSolds);
	
}

