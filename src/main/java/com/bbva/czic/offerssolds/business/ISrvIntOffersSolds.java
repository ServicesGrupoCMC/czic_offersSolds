package com.bbva.czic.offerssolds.business;

import java.util.List;
import javax.ws.rs.core.Response;


import com.bbva.czic.offerssolds.business.dto.DTOIntSellers;
import com.bbva.czic.offerssolds.business.dto.DTOIntProduct;
import com.bbva.czic.offerssolds.business.dto.DTOIntIdentityDocument;
import com.bbva.czic.offerssolds.business.dto.DTOIntDocumentType;
import com.bbva.czic.offerssolds.business.dto.DTOIntContractingChannel;
import com.bbva.czic.offerssolds.business.dto.DTOIntOffer;
import com.bbva.czic.offerssolds.business.dto.DTOIntType;
import com.bbva.czic.offerssolds.business.dto.DTOIntOffersSolds;



public interface ISrvIntOffersSolds {
 	public void createOfferSold(DTOIntOffersSolds infoOffersSolds);

	
}