
package com.bbva.czic.offerssolds.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "identityDocument", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlType(name = "identityDocument", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class IdentityDocument
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Indicators contain information of the transaction in the payment gateway", required = true)
    private DocumentType documentType;
    @ApiModelProperty(value = "Unique customer identifier.", required = true)
    private String documentNumber;
    @ApiModelProperty(value = "Identity document number.", required = true)
    private String verificationDigit;

    public IdentityDocument() {
        //default constructor
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getVerificationDigit() {
        return verificationDigit;
    }

    public void setVerificationDigit(String verificationDigit) {
        this.verificationDigit = verificationDigit;
    }

}
