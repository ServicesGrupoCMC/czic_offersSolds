
package com.bbva.czic.offerssolds.facade.v01.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "contractingChannel", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlType(name = "contractingChannel", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ContractingChannel
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Identity document information related to the seller.", required = true)
    private Type type;
    @ApiModelProperty(value = "Description product creation channel", required = true)
    private String id;

    public ContractingChannel() {
        //default constructor
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
