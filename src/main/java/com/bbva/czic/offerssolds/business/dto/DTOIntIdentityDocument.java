
package com.bbva.czic.offerssolds.business.dto;




public class DTOIntIdentityDocument {

    public final static long serialVersionUID = 1L;
    private DTOIntDocumentType documentType;
    private String documentNumber;
    private String verificationDigit;

    public DTOIntIdentityDocument() {
        //default constructor
    }

    public DTOIntDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntDocumentType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getVerificationDigit() {
        return verificationDigit;
    }

    public void setVerificationDigit(String verificationDigit) {
        this.verificationDigit = verificationDigit;
    }

}
