package com.bbva.czic.offerssolds.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.czic.offerssolds.business.dto.DTOIntOffersSolds;
import com.bbva.czic.offerssolds.business.impl.SrvIntOffersSolds;
import com.bbva.czic.offerssolds.dao.model.oz86.FormatoOZWEOZ86;
import com.bbva.czic.offerssolds.dao.model.oz86.PeticionTransaccionOz86;
import com.bbva.czic.offerssolds.dao.model.oz86.RespuestaTransaccionOz86;
import com.bbva.czic.offerssolds.dao.model.oz86.TransaccionOz86;
import com.bbva.czic.offerssolds.facade.v01.mapper.Mapper;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.Transaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.ErrorMappingHelper;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;

@Component
public class OffersSoldsDAOImpl  implements OffersSoldsDAO {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(OffersSoldsDAOImpl.class,"META-INF/spring/i18n/log/mensajesLog");
	
	@Autowired
	TransaccionOz86 oz86;
	
	@Autowired
	private ErrorMappingHelper emh;

	public void createOfferSold(DTOIntOffersSolds infoOffersSolds) {
		PeticionTransaccionOz86 peticion = new PeticionTransaccionOz86();
		CuerpoMultiparte cuerpo = new CuerpoMultiparte();
		
		FormatoOZWEOZ86 formato = Mapper.entryOz86(infoOffersSolds);
		cuerpo.getPartes().add(formato);		
		peticion.setCuerpo(cuerpo);

		RespuestaTransaccionOz86 respuesta = oz86.invocar(peticion);
		
		BusinessServiceException exception = emh.toBusinessServiceException(respuesta);

		if (exception != null) {
			throw exception;
		}
		
	}
}

