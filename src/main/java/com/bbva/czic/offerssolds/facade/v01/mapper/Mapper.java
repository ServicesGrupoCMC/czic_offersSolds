package com.bbva.czic.offerssolds.facade.v01.mapper;

import java.math.BigInteger;

import org.springframework.beans.BeanUtils;

import scala.Long;

import com.bbva.czic.offerssolds.dao.model.oz86.FormatoOZWEOZ86;
import com.bbva.czic.offerssolds.facade.v01.SrvOffersSoldsV01;
import com.bbva.czic.offerssolds.facade.v01.dto.Sellers;
import com.bbva.czic.offerssolds.facade.v01.dto.Product;
import com.bbva.czic.offerssolds.facade.v01.dto.IdentityDocument;
import com.bbva.czic.offerssolds.facade.v01.dto.DocumentType;
import com.bbva.czic.offerssolds.facade.v01.dto.ContractingChannel;
import com.bbva.czic.offerssolds.facade.v01.dto.Offer;
import com.bbva.czic.offerssolds.facade.v01.dto.Type;
import com.bbva.czic.offerssolds.facade.v01.dto.OffersSolds;
import com.bbva.czic.offerssolds.business.dto.DTOIntSellers;
import com.bbva.czic.offerssolds.business.dto.DTOIntProduct;
import com.bbva.czic.offerssolds.business.dto.DTOIntIdentityDocument;
import com.bbva.czic.offerssolds.business.dto.DTOIntDocumentType;
import com.bbva.czic.offerssolds.business.dto.DTOIntContractingChannel;
import com.bbva.czic.offerssolds.business.dto.DTOIntOffer;
import com.bbva.czic.offerssolds.business.dto.DTOIntType;
import com.bbva.czic.offerssolds.business.dto.DTOIntOffersSolds;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Mapper {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(Mapper.class,"META-INF/spring/i18n/log/mensajesLog");
	
	public static DTOIntOffersSolds OfferToDTOIntOffer(OffersSolds objOffer) {
		DTOIntOffersSolds objDtoIntOffer = new DTOIntOffersSolds();
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String jsonUser = mapper.writeValueAsString(objOffer);
			objDtoIntOffer = mapper.readValue(jsonUser, DTOIntOffersSolds.class);
		} catch (Exception e) {
			log.error("Error mapeo offer to DTOInt");
		}
		
		return objDtoIntOffer;
	}
	
	public static FormatoOZWEOZ86 entryOz86(DTOIntOffersSolds infoOffersSolds) {
		FormatoOZWEOZ86 formato = new FormatoOZWEOZ86();
		
		if (infoOffersSolds.getOffer().getProduct() != null) {
			formato.setContrat(infoOffersSolds.getOffer().getProduct().getContractId());
			formato.setTipcanl(Integer.parseInt(infoOffersSolds.getOffer().getProduct().getContractingChannel().get(0).getType().getId()));
			formato.setNomcanl(infoOffersSolds.getOffer().getProduct().getContractingChannel().get(0).getType().getName());
			//VALOR DE CANAL
			formato.setValcanl(infoOffersSolds.getOffer().getProduct().getContractingChannel().get(0).getId());
		}else{
			throw new BusinessServiceException("wrongParameters");
		}
		     
		if (infoOffersSolds.getOffer().getProduct().getContractingChannel().size() > 1) {
			//TIPO DE SUBCANAL
			formato.setTipscan(infoOffersSolds.getOffer().getProduct().getContractingChannel().get(1).getType().getId());
			
			//NOMBRE DE SUBCANAL 
			formato.setNomscan(infoOffersSolds.getOffer().getProduct().getContractingChannel().get(1).getType().getName());
			
			//VALOR DE SUBCANAL
			formato.setValscan(infoOffersSolds.getOffer().getProduct().getContractingChannel().get(1).getId());
		}				
		
		//ID DE CAMPA�A
		formato.setCampana(infoOffersSolds.getOffer().getId());
		
		//NOMBRE DE CAMPA�A  
		formato.setNomcamp(infoOffersSolds.getOffer().getName());
		
		//TIPO DE DOCUM ASES 1
		formato.setTpdocu1(infoOffersSolds.getSellers().get(0).getIdentityDocument().getDocumentType().getId());
		
		//NUM DE DOCUM ASES 1
		formato.setNmdocu1(java.lang.Long.parseLong(infoOffersSolds.getSellers().get(0).getIdentityDocument().getDocumentNumber()));
		
		//DIG DE DOCUM ASES 1
		formato.setDgdocu1(Integer.parseInt(infoOffersSolds.getSellers().get(0).getIdentityDocument().getVerificationDigit()));
		
		if (infoOffersSolds.getSellers().size() > 1) {
			formato.setTpdocu2(infoOffersSolds.getSellers().get(1).getIdentityDocument().getDocumentType().getId());
			formato.setNmdocu2(java.lang.Long.parseLong(infoOffersSolds.getSellers().get(1).getIdentityDocument().getDocumentNumber()));
			formato.setDgdocu2(Integer.parseInt(infoOffersSolds.getSellers().get(1).getIdentityDocument().getVerificationDigit()));
		}
		
		log.debug("Salio");
		
		return formato;
	}
	
}

