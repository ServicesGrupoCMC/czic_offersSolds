package com.bbva.czic.offerssolds.facade.v01;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.jaxrs.PATCH;

import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.czic.offerssolds.facade.v01.dto.Sellers;
import com.bbva.czic.offerssolds.facade.v01.dto.Product;
import com.bbva.czic.offerssolds.facade.v01.dto.IdentityDocument;
import com.bbva.czic.offerssolds.facade.v01.dto.DocumentType;
import com.bbva.czic.offerssolds.facade.v01.dto.ContractingChannel;
import com.bbva.czic.offerssolds.facade.v01.dto.Offer;
import com.bbva.czic.offerssolds.facade.v01.dto.Type;
import com.bbva.czic.offerssolds.facade.v01.dto.OffersSolds;
import com.bbva.czic.offerssolds.facade.v01.mapper.Mapper;
import com.bbva.czic.offerssolds.business.ISrvIntOffersSolds;
import com.bbva.czic.offerssolds.business.impl.SrvIntOffersSolds;


	
@Path("/V01")
@SN(registryID="SNCO1700014",logicalID="offerSolds")
@VN(vnn="V01")
@Api(value="/offersSolds/V01",description="This API includes services related to the offers available to a client. An offer corresponds to a set of information that a channel can make available to the customer to offer new products or update the products already contracted with the bank. There are also services that allow you to record the events that the client performs on the offers, then classify and analyze them, and thus provide information to the different areas on the management of the offers.")
@Produces({ MediaType.APPLICATION_JSON})
@Service

	
public class SrvOffersSoldsV01 implements ISrvOffersSoldsV01, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

	private static I18nLog log = I18nLogFactory.getLogI18n(SrvOffersSoldsV01.class,"META-INF/spring/i18n/log/mensajesLog");

	public HttpHeaders httpHeaders;
	
	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	

	public UriInfo uriInfo;
	
	@Override
	public void setUriInfo(UriInfo ui) {
		this.uriInfo = ui;		
	}

	@Override
	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}
	
	@Autowired
	ISrvIntOffersSolds srvIntOffersSolds;

	
	@ApiOperation(value="Registra los contratos vendidos por el vendedor en calidad de la informació.", notes="More text...",response=OffersSolds.class)
	@ApiResponses(value = {
		@ApiResponse(code = -1, message = "aliasGCE1"),
		@ApiResponse(code = -1, message = "aliasGCE2"),
		@ApiResponse(code = 201, message = "Added Sucessfully", response=Response.class),
		@ApiResponse(code = 500, message = "Technical Error")})
	@POST
	@Consumes({ MediaType.APPLICATION_JSON})
	@Path("/offersSolds")
	@SMC(registryID="SMCCO1720061",logicalID="createOfferSold")
	public Response createOfferSold(@ApiParam(value="Claim to add") OffersSolds infoOffersSolds) {
		srvIntOffersSolds.createOfferSold(Mapper.OfferToDTOIntOffer(infoOffersSolds));
		
		ResponseBuilder rb = Response.status(201);
		return rb.build();
	}

	

}
