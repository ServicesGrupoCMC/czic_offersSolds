package com.bbva.czic.offerssolds.dao.model.oz86;


import java.math.BigInteger;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>OZWEOZ86</code> de la transacci&oacute;n <code>OZ86</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "OZWEOZ86")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoOZWEOZ86 {

	/**
	 * <p>Campo <code>CONTRAT</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "CONTRAT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 20, longitudMaxima = 20)
	private String contrat;
	
	/**
	 * <p>Campo <code>TIPCANL</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "TIPCANL", tipo = TipoCampo.ENTERO, longitudMinima = 2, longitudMaxima = 2)
	private Integer tipcanl;
	
	/**
	 * <p>Campo <code>NOMCANL</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 3, nombre = "NOMCANL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String nomcanl;
	
	/**
	 * <p>Campo <code>VALCANL</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 4, nombre = "VALCANL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String valcanl;
	
	/**
	 * <p>Campo <code>TIPSCAN</code>, &iacute;ndice: <code>5</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 5, nombre = "TIPSCAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String tipscan;
	
	/**
	 * <p>Campo <code>NOMSCAN</code>, &iacute;ndice: <code>6</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 6, nombre = "NOMSCAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String nomscan;
	
	/**
	 * <p>Campo <code>VALSCAN</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 7, nombre = "VALSCAN", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 2, longitudMaxima = 2)
	private String valscan;
	
	/**
	 * <p>Campo <code>CAMPANA</code>, &iacute;ndice: <code>8</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 8, nombre = "CAMPANA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 12, longitudMaxima = 12)
	private String campana;
	
	/**
	 * <p>Campo <code>NOMCAMP</code>, &iacute;ndice: <code>9</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 9, nombre = "NOMCAMP", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String nomcamp;
	
	/**
	 * <p>Campo <code>TPDOCU1</code>, &iacute;ndice: <code>10</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 10, nombre = "TPDOCU1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String tpdocu1;
	
	/**
	 * <p>Campo <code>NMDOCU1</code>, &iacute;ndice: <code>11</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 11, nombre = "NMDOCU1", tipo = TipoCampo.ENTERO, longitudMinima = 15, longitudMaxima = 15)
	private Long nmdocu1;
	
	/**
	 * <p>Campo <code>DGDOCU1</code>, &iacute;ndice: <code>12</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 12, nombre = "DGDOCU1", tipo = TipoCampo.ENTERO, longitudMinima = 1, longitudMaxima = 1)
	private Integer dgdocu1;
	
	/**
	 * <p>Campo <code>TPDOCU2</code>, &iacute;ndice: <code>13</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 13, nombre = "TPDOCU2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 3, longitudMaxima = 3)
	private String tpdocu2;
	
	/**
	 * <p>Campo <code>NMDOCU2</code>, &iacute;ndice: <code>14</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 14, nombre = "NMDOCU2", tipo = TipoCampo.ENTERO, longitudMinima = 15, longitudMaxima = 15)
	private Long nmdocu2;
	
	/**
	 * <p>Campo <code>DGDOCU2</code>, &iacute;ndice: <code>15</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 15, nombre = "DGDOCU2", tipo = TipoCampo.ENTERO, longitudMinima = 1, longitudMaxima = 1)
	private Integer dgdocu2;
	
}