
package com.bbva.czic.offerssolds.business.dto;




public class DTOIntSellers {

    public final static long serialVersionUID = 1L;
    private String fullname;
    private DTOIntIdentityDocument identityDocument;

    public DTOIntSellers() {
        //default constructor
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public DTOIntIdentityDocument getIdentityDocument() {
        return identityDocument;
    }

    public void setIdentityDocument(DTOIntIdentityDocument identityDocument) {
        this.identityDocument = identityDocument;
    }

}
