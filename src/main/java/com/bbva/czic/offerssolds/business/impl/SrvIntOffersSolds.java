package com.bbva.czic.offerssolds.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;

import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.czic.offerssolds.business.dto.DTOIntSellers;
import com.bbva.czic.offerssolds.business.dto.DTOIntProduct;
import com.bbva.czic.offerssolds.business.dto.DTOIntIdentityDocument;
import com.bbva.czic.offerssolds.business.dto.DTOIntDocumentType;
import com.bbva.czic.offerssolds.business.dto.DTOIntContractingChannel;
import com.bbva.czic.offerssolds.business.dto.DTOIntOffer;
import com.bbva.czic.offerssolds.business.dto.DTOIntType;
import com.bbva.czic.offerssolds.business.dto.DTOIntOffersSolds;
import com.bbva.czic.offerssolds.business.ISrvIntOffersSolds;
import com.bbva.czic.offerssolds.dao.OffersSoldsDAO;


@Service
public class SrvIntOffersSolds implements ISrvIntOffersSolds {
	
	private static I18nLog log = I18nLogFactory.getLogI18n(SrvIntOffersSolds.class,"META-INF/spring/i18n/log/mensajesLog");

	@Autowired
	BusinessServicesToolKit bussinesToolKit;
	
	@Autowired
	OffersSoldsDAO dao;
	
	@Override
	public void createOfferSold(DTOIntOffersSolds infoOffersSolds) {
		dao.createOfferSold(infoOffersSolds);
	}

	

}
