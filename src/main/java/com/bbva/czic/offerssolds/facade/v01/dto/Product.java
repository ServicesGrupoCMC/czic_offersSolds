
package com.bbva.czic.offerssolds.facade.v01.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "product", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlType(name = "product", namespace = "urn:com:bbva:czic:offerssolds:facade:v01:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Product
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique customer identifier.", required = true)
    private String contractId;
    @ApiModelProperty(value = "Identity document information related to the seller.", required = true)
    private List<ContractingChannel> contractingChannel;

    public Product() {
        //default constructor
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public List<ContractingChannel> getContractingChannel() {
        return contractingChannel;
    }

    public void setContractingChannel(List<ContractingChannel> contractingChannel) {
        this.contractingChannel = contractingChannel;
    }

}
