package com.bbva.czic.offerssolds.dao.model.oz86;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>OZ86</code>
 * 
 * @see PeticionTransaccionOz86
 * @see RespuestaTransaccionOz86
 */
@Component
public class TransaccionOz86 implements InvocadorTransaccion<PeticionTransaccionOz86,RespuestaTransaccionOz86> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionOz86 invocar(PeticionTransaccionOz86 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOz86.class, RespuestaTransaccionOz86.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionOz86 invocarCache(PeticionTransaccionOz86 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionOz86.class, RespuestaTransaccionOz86.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}